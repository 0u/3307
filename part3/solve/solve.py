from telnetlib import Telnet
import re, json
from timeit import default_timer as timer

# wX&tF3czE!\t$fcPU^?(!izb3
password = 'wX&tF3czE!\\t$fcPU^?(!izb3'

def is_prime(n):
  if n == 2 or n == 3: return True
  if n < 2 or n%2 == 0: return False
  if n < 9: return True
  if n%3 == 0: return False
  r = int(n**0.5)
  f = 5
  while f <= r:
    if n%f == 0: return False
    if n%(f+2) == 0: return False
    f +=6
  return True

n_primes = 100000

with open('primes-100k.json') as f:
    primes = json.load(f)
    print('[+] Loaded %d primes.' % len(primes))

regexes = [
    b'What is the (\d+)th prime number \(counting from 1\)\n',
    b'What is (\d+) times (\d+)\n',
]

print('[*] Connecting... ', end = '')
# with Telnet('127.0.0.1', 3307) as tn:
with Telnet('sbzuqefrycksmtr6fd76x6ydjgz6noibadeb2gaseunvrf5qyc4p44ad.onion', 3307) as tn:
    print('Done')
    tn.read_until(b"Password: ")
    tn.write(password.encode('ascii') + b"\n")

    for _ in range(13):
        i, match, b = tn.expect(regexes, timeout = 6)
        print(b.decode())
        if i == 0:
            i = int(match.group(1).decode())
            s = str(primes[i-1])
            print(s)
            tn.write((s + "\n").encode())
        elif i == 1:
            x, y = match.group(1), match.group(2)
            x, y = int(x.decode()), int(y.decode())
            s = str(x * y)
            print(s)
            tn.write((s + '\n').encode())

    tn.interact()

