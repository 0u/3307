// gcc -lm calc-primes.c -o calc-primes

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

void die(char* msg) {
  fprintf(stderr, "%s\n", msg);
  exit(1);
}

bool is_prime(int n) {
  /* Quick and easy cases */
  if(n == 2 || n == 3) return true;
	if(n < 2 || n%2 == 0) return false;
	if(n < 9) return true;
	if(n%3 == 0) return false;

	/* Do the loop */
  for (int i = 2; i <= sqrt(n); i++) {

      // If n is divisible by any number between
      // 2 and n/2, it is not prime
      if (n % i == 0) {
        return false;
      }
  }

	return true;
}

int main(int argc, char const* argv[])
{

  FILE* f = fopen("./primes-100k.json", "w");
  if (f == NULL) die("Failed to open file for writing");
  fprintf(f, "[2");

  int i = 3;
  for (int n = 1; n < 100000;) {
    if(is_prime(i)) {
      fprintf(f, ",%d", i);
      n++;
    }
    i++;
  }

  fprintf(f, "]");

  return 0;
}
