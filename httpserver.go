package main

import (
	"io"
	"log"
	"net/http"
	"os/exec"
	"strings"
	"flag"
)

var port = flag.String("port", ":8080", "HTTP port")

func passed(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "text/html")
	io.WriteString(w, `<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>3307</title>
    <link rel=stylesheet href="/style.css">
  </head>
  <body>
    <p>Connect to us on port 3307</p>
	<p>The key is wX&tF3czE!\t$fcPU^?(!izb3</p>
    <p>Good luck.</p>
    <p>- 3307</p>
  </body>
</html>
`)
}

func uptimeHandler(w http.ResponseWriter, r *http.Request) {
	values := r.URL.Query()
	args := values.Get("args")

	if strings.Contains(args, ";") || strings.Contains(args, "|") {
		passed(w, r)
		return
	}

	cmd := exec.Command("uptime", args)
	cmd.Stdout = w
	cmd.Stderr = w
	if err := cmd.Run(); err != nil {
		log.Printf("Failed to run 'uptime': %v", err)
	}

	w.Header().Set("content-type", "text/plain")
}

func main() {
	flag.Parse()
	go part3()

	fs := http.FileServer(http.Dir("./public"))
	http.Handle("/", fs)
	http.HandleFunc("/cgi-bin/uptime.cgi", uptimeHandler)
	http.HandleFunc("/ask-an-answer-or-get-a-question", part4)

	log.Printf("Listening on %s (http)", *port)
	panic(http.ListenAndServe(*port, nil))
}
