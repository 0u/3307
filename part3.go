package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"math"
	"math/rand"
	"net"
	"os"
	"strconv"
	"time"
)

const bind = "127.0.0.1:3307"

func p(conn net.Conn, s string, a ...interface{}) {
	s = fmt.Sprintf(s, a...)

	for _, char := range s {
		_, err := io.WriteString(conn, string(char))
		if err != nil {
			panic(err)
		}
		time.Sleep(time.Millisecond * 50)
	}
}

// no fucking idea what this does just copied from stackoverflow
// and translated it from python to go
func isPrime(n int) bool {
	if n == 2 || n == 3 {
		return true
	}
	if n < 2 || n%2 == 0 {
		return false
	}
	if n < 9 {
		return true
	}
	if n%3 == 0 {
		return false
	}
	r := int(math.Pow(float64(n), 0.5))
	f := 5
	for f <= r {
		if n%f == 0 {
			return false
		}
		if n%(f+2) == 0 {
			return false
		}
		f += 6
	}
	return true
}

func init() {
	log.Printf("Generating primes table...")
	primes = make([]int, primesLen)

	i := 0
	for j := 0; ; j++ {
		if isPrime(j) {
			primes[i] = j
			i++
			if i >= primesLen {
				break
			}
		}
	}
	log.Printf("Done, len: %d, cap: %d", len(primes), cap(primes))
}

const primesLen = 100000

var primes []int

var questions = []func(net.Conn, *bufio.Scanner) bool{
	func(conn net.Conn, s *bufio.Scanner) bool {
		nth := rand.Intn(primesLen)
		p(conn, "What is the %dth prime number (counting from 1)\n=> ", nth+1)
		conn.SetReadDeadline(time.Now().Add(time.Second * 3))
		if s.Scan() {
			if s.Text() == strconv.Itoa(primes[nth]) {
				p(conn, "Correct.\n")
				return true
			}
			p(conn, "\nIncorrect. The answer was %d\n", primes[nth])
		} else {
			p(conn, "\nTimed out. The answer was %d\n", primes[nth])
		}
		return false
	},

	func(conn net.Conn, s *bufio.Scanner) bool {
		x, y := rand.Intn(3307), rand.Intn(3307)
		correct := strconv.Itoa(x * y)
		fmt.Fprintf(os.Stderr, "%d * %d = %s\n", x, y, correct)
		p(conn, "What is %d times %d\n=> ", x, y)
		conn.SetReadDeadline(time.Now().Add(time.Second * 3))
		if s.Scan() {
			if s.Text() == correct {
				p(conn, "Correct.\n")
				return true
			}
			p(conn, "\nIncorrect. The answer was %s\n", correct)
		} else {
			p(conn, "\nTimed out. The answer was %s\n", correct)
		}
		return false
	},
}

func handle(conn net.Conn) {
	const password = `wX&tF3czE!\t$fcPU^?(!izb3`
	s := bufio.NewScanner(conn)

	for p(conn, "Password: "); s.Scan(); p(conn, "Password: ") {
		if s.Text() == password {
			p(conn, "Access granted.\n")
			break
		} else {
			p(conn, "Access denied.\n")
			return
		}
	}

	/*
		p(conn, "\n===================================================\n")
		p(conn, "You will receive a series of questions too fast for a human to answer.\n")
		p(conn, "You must write a program to answer the questions.\n")
		p(conn, "Good luck.\n")
		p(conn, "- 3307\n")
		p(conn, "===================================================\n\n")
	*/
	p(conn, "Answer me my riddles 13\n")
	p(conn, "Time though will be scare indeed\n")

	for i := 0; i < 13; i++ {
		// now for the tricky bit
		if !questions[rand.Intn(len(questions))](conn, s) {
			return
		}
	}

	p(conn, "/SjZ4KHXqtBluQXDy74Lg7me7D.html\n")
}

func run() error {
	rand.Seed(time.Now().Unix())

	l, err := net.Listen("tcp", bind)
	if err != nil {
		return err
	}
	log.Print("Listening on " + bind)

	for errc := 0; errc < 10; {
		conn, err := l.Accept()
		if err != nil {
			errc++
			log.Printf("Failed to accept: %v", err)
			continue
		}

		go func() {
			defer func() {
				if e := recover(); e != nil {
					log.Printf("[panic] %v", e)
				}
			}()
			handle(conn)
			if err := conn.Close(); err != nil {
				log.Printf("Failed to close: %v", err)
			}
		}()
	}

	return errors.New("too many accept errors")
}

func part3() {
	for {
		if err := run(); err != nil {
			log.Printf("[run] %v", err)
		}
		time.Sleep(time.Millisecond * 100)
	}
}
