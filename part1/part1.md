Hello. We are looking for highly intelligent individuals.
To find them, we have devised a test.

the following string is encrypted using the following function

```py
def enc(s):
    s = codecs.encode(s, 'rot_13')

    for k in 'A3 A7 AD B3 B5 BF C1 C5 C7 D3 DF E3 E5 E9 EF F1 FB'.split(' '):
        k = int('0x' + k, 16)
        s = (''.join(chr(ord(c) ^ k) for c in s))

    return s
```

Reverse the cipher and decrypt
```
wrPCusK4wr3CscKnwqbCsMK5wqXCrcKzwq/CssKww6PCpsKkw6LDo8K+w6PCucKkwqLCocK4w6PCtMK3wqPCusK7wqTCp8K6w6fCocK7wrPCp8K9wrTCvMKwwqbDoMKxwrnCpcOhwrbDocOhwrvCpMO7wrfCtMKjwrfCtA==
```

Do this and it will lead you on the road to finding us. We look forward to meeting the few who will make it all the way through.

Good Luck.

- 3307
