import codecs, base64

def enc(s):
    s = codecs.encode(s, 'rot_13')

    for k in 'A3 A7 AD B3 B5 BF C1 C5 C7 D3 DF E3 E5 E9 EF F1 FB'.split(' '):
        k = int('0x' + k, 16)
        s = (''.join(chr(ord(c) ^ k) for c in s))

    return s

s = enc('nhaz4g4djr77qif74lecyfitgsg3rcmih7it6lnjjookvyolk7gjdoqd.onion')

print(base64.b64encode(s.encode()).decode())

def dec(s):
    for k in reversed('A3 A7 AD B3 B5 BF C1 C5 C7 D3 DF E3 E5 E9 EF F1 FB'.split(' ')):
        k = int('0x' + k, 16)
        s = (''.join(chr(ord(c) ^ k) for c in s))


    return codecs.decode(s, 'rot_13')

# in honor of goopsie's stupid solution because he did not know what a list comprehension was.
def dec_goopsie(s):
    transition_table = {}
    for c in (chr(n) for n in range(256)):
        transition_table[enc(c)] = c

    return ''.join(transition_table[c] for c in s)

print(dec('wrTCoMK7wrjDocKhw6HCpMKiwrDDosOiwrHCo8Kmw6LDocKswqfCpcK5wqbCo8KywqHCs8Khw6bCsMKlwq/Co8Kgw6LCo8Kyw6PCrMK0wqLCosK3wrfCrcK8wrnCt8Kswq3DosKhwqLCpMK3wrHCpMO7wrfCtMKjwrfCtA=='))
print(dec_goopsie(s))
assert dec(s) == dec_goopsie(s)

