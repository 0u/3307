package main

import (
	"io"
	"net/http"
)

const wantSum = 160317681291

func valid(data []byte) bool {
	var sum uint64

	for _, b := range data {
		sum += (sum ^ uint64(b))
	}

	return sum == wantSum
}

func part4(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.WriteHeader(405)
		io.WriteString(w, "Method not allowed.\n")
		return
	}

	/*// technically max size for token is 32, but i'm lazy and I don't feel like debugging off by one errors.
	buf := make([]byte, 64)

	n, err := r.Body.Read(buf)
	if err != nil && err != io.EOF {
		w.WriteHeader(500)
		io.WriteString(w, "Failed to read body\n")
		return
	}*/
	if err := r.ParseForm(); err != nil {
		w.WriteHeader(400)
		io.WriteString(w, "Bad request.\n")
		return
	}

	// chop off "key=" (4 chars)
	// this is terrible but not any worse than the rest of this codebase
	if valid([]byte(r.Form.Get("key"))) {
		io.WriteString(w, "You did it! Submit your solutions to uli#4334 on the 3307 discord for the solver role. (https://discord.gg/ZQNmxME)\n")
	} else {
		io.WriteString(w, "Incorrect!\n")
	}

}
