// compile with: 
// gcc part4.c -O1 -o public/part4

#include <stdio.h>
#include <string.h>

// Random sum, they need to figure out how to manipulate the binary into giving them a valid sum.
#define WANT_SUM 160317681291
#define u64 unsigned long int
// 2c4*qk>T,i]5a2"u;oS%lfMDq\P50QdC
#define KEY "2c4*qk>T,i]5a2\"u;oS%lfMDq\\P50QdC"

// Custom checksum function
// TODO: Rewrite with real mathimatically sound checksum.
u64 calc_sum(char* key) {
  u64 sum = 0;
  for (int i = 0; i < strlen(key); i++) {
    sum += (sum ^ key[i]);
  }

  return sum;
}

int main(int argc, char* argv[]) {
  if (argc != 2 || strlen(argv[1]) != 32) {
    fprintf(stderr, "Usage: %s <32 byte license key>\n", argv[0]);
    return 1;
  }

  // printf("%lu\n", crc32(0, (void*)argv[1], strlen(argv[1])));
  u64 sum = calc_sum(argv[1]);
  if (sum == WANT_SUM) {
    printf("Valid license key!\n");
    return 0;
  } else {
    /* fprintf(stderr, "Access denied, want sum %lu, got sum %lu\n", WANT_SUM, sum); */
    printf("Invalid license key!\n");
    return 1;
  }
}
