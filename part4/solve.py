import codecs, os, string, random
WANT_SUM = 160317681291
# CHARSET = string.printable.strip()
CHARSET = string.ascii_lowercase

def randstr():
    return ''.join(random.choices(CHARSET, k=32))

calls = 0

# Custom checksum function
def calc_sum(key: str):
  global calls
  calls += 1

  t = 0
  for c in key:
    t += (t ^ ord(c))

  return t

def ch(s, i, c):
    "Change s[i] to c and return the new string with the modification made."
    return s[:i] + c + s[i+1:]

class Best:
    def __init__(self, s):
        self.s = s
        self.dist = abs(calc_sum(s) - WANT_SUM)

    @classmethod
    def rand(cls):
        return cls(randstr())



def optimize_from(best: Best):
    print('[*] Begin optimization from distance {}'.format(best.dist))

    while True:
        best_prev_pass = best
        for i in range(len(best.s)):
            for c in string.printable:
                new = Best(ch(best.s, i, c))
                if new.dist < best.dist:
                    best = new
                    print('[*] New best: {}'.format(best.dist))

                    if best.dist == 0:
                        print('[!] Found answer: {} (took {} calls)'.format(best.s, calls))
                        return best.s

        if best.dist == best_prev_pass.dist:
            print('[!] Got stuck! restarting...')
            return None


def solve():
    """
    To solve we use the property that changes
    in the beginning of the key will have far greater
    affects then changes at the end.

    what we do is
    1. Try a few thousand guesses and settle on the closest result.
    2. Optimize the closest result.
    3. profit
    """
    # Optimize from the beginning onwards. in loops
    while True:
        x = optimize_from(Best.rand())
        if x is not None:
            return x

if __name__ == '__main__':
    solve()
