package main

import "testing"

func TestValid(t *testing.T) {
	tests := map[string]bool{
		`2c4*qk>T,i]5a2"u;oS%lfMDq\P50QdC`:   true,
		`2c4*qk>T,i]ga2"u;oS%lfMDq\P50QdC`:   false,
		`184uraf9sjh`:                        false,
		"a;$U0la!I\\b1@8ia:v6At3QM0r8AFB\ts": true,
		`4{Kkq,\wAeM1O4*pcC+00=Pu1#l6Aqbc`:   true,
	}

	for text, want := range tests {
		if valid([]byte(text)) != want {
			t.Fatalf("want %q to be %v", text, want)
		}
	}
}
