#!/usr/bin/python
from urllib.parse import parse_qs
import traceback, os

print('Content-type: text/plain\n', flush=True)

query_strings = parse_qs(os.getenv('QUERY_STRING'))
args = query_strings.get('args')
if args is not None:
    os.system("uptime %s" % args[0])
else:
    os.system("uptime")
